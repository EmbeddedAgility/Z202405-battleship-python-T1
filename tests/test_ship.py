from unittest import TestCase
from torpydo.ship import *


class TestShip(TestCase):

    def test_add_position_position(self):
        ship = Ship("test", 3, Color.RED)
        assert len(ship.positions) == 0
        ship.add_position("A1")
        assert len(ship.positions) == 1
        ship.add_position("A2")
        assert len(ship.positions) == 2
        assert ship.orientation == Orientation.VERTICAL
        ship.add_position("A3")
        assert len(ship.positions) == 3

    def test_validate_position_not_used_by_sef(self):
        ship = Ship("test", 3, Color.RED)
        assert len(ship.positions) == 0
        ship.add_position("A1")
        assert len(ship.positions) == 1
        ship.add_position("A1")
        assert len(ship.positions) == 1

    def test_validate_position_adjacent(self):
        ship = Ship("test", 3, Color.RED)
        assert len(ship.positions) == 0
        ship.add_position("A1")
        assert len(ship.positions) == 1
        ship.add_position("A3")
        assert len(ship.positions) == 1
        ship.add_position("C1")
        assert len(ship.positions) == 1
        ship.add_position("A2")
        assert len(ship.positions) == 2
        ship.positions = ship.positions[:1]
        ship.add_position("B1")
        assert len(ship.positions) == 2

    def test_set_orientation_vertical(self):
        ship = Ship("test", 3, Color.RED)
        ship.add_position("A1")
        ship.add_position("A2")
        assert ship.orientation == Orientation.VERTICAL

    def test_set_orientation_horizontal(self):
        ship = Ship("test", 3, Color.RED)
        ship.add_position("A1")
        ship.add_position("B1")
        assert ship.orientation == Orientation.HORIZONTAL
