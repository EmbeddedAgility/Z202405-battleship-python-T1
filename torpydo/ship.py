from enum import Enum
from colorama import Fore

class Color(Enum):
    CADET_BLUE = 1
    CHARTREUSE = 2
    ORANGE = 3
    RED = 4
    YELLOW = 5

class Letter(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    G = 7
    H = 8



class Orientation(Enum):
    HORIZONTAL = 1
    VERTICAL = 2


class Position(object):
    def __init__(self, column: Letter, row: int):
        self.column = column
        self.row = row

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return f"{self.column.name}{self.row}"

    __repr__ = __str__

class Ship(object):
    def __init__(self, name: str, size: int, color: Color):
        self.name = name
        self.size = size
        self.color = color
        self.positions = []

        self.orientation = None

    def add_position(self, input: str) -> bool:
        try:
            letter = Letter[input.upper()[:1]]
            number = int(input[1:])
            if number not in [1, 2, 3, 4, 5, 6, 7, 8]:
                raise Exception("Expection")
            position = Position(letter, number)
            if self.validate_position(position):
                self.positions.append(Position(letter, number))
                return True
        except (KeyError, ValueError, Exception):
            print(Fore.GREEN + "Invalid input. Please enter a valid position (e.g., A3).")
            return False

    def validate_position(self, position: Position):
        if not self.validate_position_has_same_orientation(position):
            return False
        if not self.validate_position_not_used_by_sef(position):
            return False
        if not self.validate_position_is_adjacent(position):
            return False
        return True

    def validate_position_has_same_orientation(self, position: Position):
        if len(self.positions) == 0:
            return True

        vertical = True
        horizontal = True

        for existingPos in self.positions:
            if position.row != existingPos.row:
                horizontal = False
            if position.column != existingPos.column:
                vertical = False

        if (len(self.positions) == 1):
            if (vertical):
                self.orientation = Orientation.VERTICAL
                return True
            if (horizontal):
                self.orientation = Orientation.HORIZONTAL
                return True

        if(vertical or horizontal):
            return True

        print("Postion {pos} is invalid".format(pos = position))
        return False

    def validate_position_not_used_by_sef(self, position: Position):
        if len(self.positions) == 0:
            return True

        for existingPos in self.positions:
            if existingPos.column == position.column and existingPos.row == position.row:
                print("Position {pos} is already used by this ship".format(pos=position))
                return False
        return True

    def validate_position_is_adjacent(self, position: Position):
        if len(self.positions) == 0:
            return True

        if (len(self.positions) == 1):
            existingPosition = self.positions[0]
            isAdjacent = self.is_adjacent_vertically(existingPosition, position) or self.is_adjacent_horizontally(
                existingPosition, position)
            if (isAdjacent):
                return True
            print("Position {newPos} is not adjacent to existing position {pos}".format(newPos = position, pos = existingPosition))
            return False

        if (self.orientation == Orientation.VERTICAL):
            firstPosition = self.positions[0]
            lastPosition = self.positions[len(self.positions) - 1]
            isAdjacent = self.is_adjacent_vertically(firstPosition, position) or self.is_adjacent_vertically(lastPosition, position)
            if (isAdjacent):
                return True
            print("Position {newPos} is not adjacent to current ends of boat {first}, {last}".format(newPos = position, first = firstPosition, last = lastPosition))
            return False

        if (self.orientation == Orientation.HORIZONTAL):
            firstPosition = self.positions[0]
            lastPosition = self.positions[len(self.positions) - 1]
            isAdjacent = self.is_adjacent_horizontally(firstPosition, position) or self.is_adjacent_horizontally(lastPosition, position)
            if (isAdjacent):
                return True
            print("Position {newPos} is not adjacent to current ends of boat {first}, {last}".format(newPos = position, first = firstPosition, last = lastPosition))
            return False

        return False

    def is_adjacent_vertically(self, position1: Position, position2: Position):
        return abs(position1.row - position2.row) == 1

    def is_adjacent_horizontally(self, position1: Position, position2: Position):
        return abs(position1.column.value - position2.column.value) == 1



    def reduce_size(self):
        if self.size > 0:
            self.size =  self.size -1


    def __str__(self):
        return f"{self.color.name} {self.name} ({self.size}): {self.positions}"

    __repr__ = __str__
