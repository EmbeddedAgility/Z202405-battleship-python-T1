import random
import os
import colorama
import platform

import pandas as pd
import numpy as np

# Pipleine
from colorama import Fore, Back, Style
from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.telemetryclient import TelemetryClient

print("Starting")

myFleet = []
enemyFleet = []


def main():
    TelemetryClient.init()
    TelemetryClient.trackEvent('ApplicationStarted', {'custom_dimensions': {'Technology': 'Python'}})
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
__..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
\_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():



    global myFleet, enemyFleet
    # clear the screen
    if(platform.system().lower()=="windows"):
        cmd='cls'
    else:
        cmd='clear'   
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')


    while True:
        print()
        print(Back.WHITE)
        print_player_message("Player, it's your turn")

        ships_not_sunk = [i.name for i in enemyFleet if i.size > 0]
        if ships_not_sunk:
            print_player_message("Ships that are not sunk are: "+', '.join(ships_not_sunk))

        position = parse_position(input(Fore.GREEN+"Enter coordinates for your shot :"))
        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:
            get_value = computer_postions_dict.at[position.row, position.column.name]
            if get_value == -1:
                is_hit = False
            else:
                computer_postions_dict.at[position.row, position.column.name] = -1
                enemyFleet[get_value].reduce_size()
                print(Fore.RED+r'''
                    \          .  ./
                \   .:"";'.:..""   /
                    (M^^.^~~:.'"").
                -   (/  .    . . \ \)  -
                ((| :. ~ ^  :. .|))
                -   (\- |  \ /  |  /)  -
                    -\  \     /  /-
                    \  \   /  /''')

                ships_sunk = [i.name for i in enemyFleet if i.size == 0]
                if ships_sunk:
                    print(Fore.LIGHTMAGENTA_EX+"Ships that are sunk are: "+', '.join(ships_sunk))
                if len(ships_sunk) == len(enemyFleet):
                    print_player_message("CONGRATS! YOU WON THE GAME!")
                    return

        print_text_with_color(Fore.YELLOW, 'Yeah ! Nice hit !') if is_hit else  print_text_with_color(Fore.BLUE, 'Miss! You hit the water!')
        TelemetryClient.trackEvent('Player_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})

        print_computer_message("It's computers turn")
        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print_text_with_color(Fore.YELLOW, 'Computer shoot in {} and hit your ship'.format(str(position))) if is_hit else \
                              print_text_with_color(Fore.BLUE, 'Computer shoot in {} and miss your ship'.format(str(position)))

        TelemetryClient.trackEvent('Computer_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if is_hit:

            get_value = my_postions_dict.at[position.row, position.column.name]
            if get_value == -1:
                is_hit = False
            else:
                my_postions_dict.at[position.row, position.column.name] = -1
                myFleet[get_value].reduce_size()
                print(Fore.RED+r'''
                    \          .  ./
                \   .:"";'.:..""   /
                    (M^^.^~~:.'"").
                -   (/  .    . . \ \)  -
                ((| :. ~ ^  :. .|))
                -   (\- |  \ /  |  /)  -
                    -\  \     /  /-
                    \  \   /  /''')

                ships_sunk = [i.name for i in myFleet if i.size == 0]
                if ships_sunk:
                    print(Fore.LIGHTMAGENTA_EX+"Ships that are sunk are: "+', '.join(ships_sunk))
                if len(ships_sunk) == len(myFleet):
                    print_player_message("GAME OVER! COMUTER WON!")
                    return


def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet
    global my_postions_dict

    myFleet = GameController.initialize_ships()

    my_postions_dict = pd.DataFrame({
        'A': np.full(8, -1),
        'B': np.full(8, -1),
        'C': np.full(8, -1),
        'D': np.full(8, -1),
        'E': np.full(8, -1),
        'F': np.full(8, -1),
        'G': np.full(8, -1),
        'H': np.full(8, -1),
    }, index=[1,2,3,4,5,6,7,8])

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        while ship.size != len(ship.positions):
            position_input = input(f"Enter position {len(ship.positions) + 1} of {ship.size} (i.e A3):")

            if(is_position_available(position_input)):
                ship.add_position(position_input)

            letter = Letter[position_input.upper()[:1]]
            number = int(position_input[1:])

            my_postions_dict.at[number, letter.name] = int(myFleet.index(ship))
            TelemetryClient.trackEvent('Player_PlaceShipPosition', {'custom_dimensions': {'Position': position_input, 'Ship': ship.name, 'PositionInShip': len(ship.positions) + 1}})
            print(my_postions_dict)

def is_position_available(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    if number not in [1, 2, 3, 4, 5, 6, 7, 8]:
        raise Exception("Expection")
    position = Position(letter, number)
    for ship in myFleet:
        for otherShipPosition in ship.positions:
            if(otherShipPosition == position):
                print("Position {pos} is already used by ship {otherShip}".format(pos = position, otherShip = ship.name))
                return False
    return True


def initialize_enemyFleet():
    global enemyFleet
    global computer_postions_dict

    import pandas as pd
    import numpy as np

    computer_postions_dict = pd.DataFrame({
        'A': np.full(8, -1),
        'B': np.full(8, -1),
        'C': np.full(8, -1),
        'D': np.full(8, -1),
        'E': np.full(8, -1),
        'F': np.full(8, -1),
        'G': np.full(8, -1),
        'H': np.full(8, -1),
    }, index=[1,2,3,4,5,6,7,8])



    """
    AC  [0]
    B   [1]
    S   [2]
    D   [3]
    PB  [4]
    """


    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    computer_postions_dict.at[4, 'B'] = 0
    computer_postions_dict.at[5, 'B'] = 0
    computer_postions_dict.at[6, 'B'] = 0
    computer_postions_dict.at[7, 'B'] = 0
    computer_postions_dict.at[8, 'B'] = 0

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 5))

    computer_postions_dict.at[6, 'E'] = 1
    computer_postions_dict.at[7, 'E'] = 1
    computer_postions_dict.at[8, 'E'] = 1
    computer_postions_dict.at[5, 'E'] = 1

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    computer_postions_dict.at[3, 'A'] = 2
    computer_postions_dict.at[3, 'B'] = 2
    computer_postions_dict.at[3, 'C'] = 2

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    computer_postions_dict.at[8, 'F'] = 3
    computer_postions_dict.at[8, 'G'] = 3
    computer_postions_dict.at[8, 'H'] = 3


    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

    computer_postions_dict.at[5, 'C'] = 4
    computer_postions_dict.at[6, 'C'] = 4

def print_text_with_color(color,text: str):
    print(color+text+Style.RESET_ALL)

def print_player_message(text: str):
    print(Fore.GREEN+text)

def print_computer_message(text: str):
    print(Fore.RED+text)

if __name__ == '__main__':
    main()
